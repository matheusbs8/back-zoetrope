const express = require('express');
require('./config/dotenv')();
require('./config/sequelize');

const app = express();
const port = process.env.PORT;
//const cors = require('cors');
const routes = require('./routes/routes');


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(routes);


app.get('/', (req, res) => {
  res.send('<h1 style="text-align:center">Seja bem vindo ao Zoetrope<br> o lugar onde o filme não acaba<h1>')
});

app.listen(port, () => {
  console.log(`${process.env.APP_NAME} app listening at http://localhost:${port}`);
});
    