const { response } = require('express');
const User = require('../models/User');
const Midialist = require('../models/Midialist');

const index = async(req,res) => {
    try {
        const midialist = await Midialist.findAll();
        return res.status(200).json({midialist});
    }catch(err){
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try{
          const midialist = await Midialist.create(req.body);
          return res.status(201).json({message: "Midialist cadastrado com sucesso!", midialist: midialist});
      }catch(err){
          res.status(500).json(err+'!');
      }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const midialist = await Midialist.findByPk(id);
        return res.status(200).json({midialist});
    }catch(err){
        return res.status(500).json({err});
    }
};


const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Midialist.update(req.body, {where: {id: id}});
        if(updated) {
            const midialist = await Midialist.findByPk(id);
            return res.status(200).send(midialist);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Midialist não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Midialist.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Midialist deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Midialist não encontrado.");
    }
};

const addRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const midialist = await Midialist.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await midialist.setUser(user);
        return res.status(200).json({user, midialist});

    }catch(err){
        return res.status(500).json(err+'!');
    }
};

const removeRelationUser = async(req,res) => {
    const {id} = req.params;
    try {
        const midialist = await Midialist.findByPk(id);
        await midialist.setUser(null);
        return res.status(200).json(midialist);
    }catch(err){
        return res.status(500).json(err+'!');
    }
}


module.exports = {
    index,
    create,
    show,
    update,
    destroy,
    addRelationUser,
    removeRelationUser

};
