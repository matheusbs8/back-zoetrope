const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Midialist = sequelize.define('Midialist', {
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    creation_date:{
        type: DataTypes.DATE
    },
    
});

Midialist.associate = function(models){
    Midialist.belongsTo(models.User,{ });
}

module.exports=Midialist;