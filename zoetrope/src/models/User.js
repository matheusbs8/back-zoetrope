const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    phone_number:{
        type: DataTypes.STRING
    },
    password:{
        type: DataTypes.STRING
    },
    
});

User.associate = function(models){
    User.hasMany(models.Midialist,{ });
}

module.exports=User;