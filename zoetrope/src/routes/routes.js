const { Router } = require('express');
const UserController = require('../controllers/UserController');
const MidialistController = require('../controllers/MidialistController');


const router = Router();

router.get('/user',UserController.index);
router.get('/user/:id',UserController.show);
router.post('/user',UserController.create);
router.get('/user/:id',UserController.show);
router.put('/user/:id', UserController.update);

router.delete('/user/:id', UserController.destroy);


router.get('/midialist',MidialistController.index);
router.post('/midialist',MidialistController.create);
router.get('/midialist/:id',MidialistController.show);
router.put('/midialist/:id', MidialistController.update);
router.put('/midialistadduser/:id', MidialistController.addRelationUser);
router.delete('/midialistremoveuser/:id', MidialistController.removeRelationUser);
router.delete('/midialist/:id', MidialistController.destroy);

module.exports = router;
